import logging
from logging import info, error, exception, warning
logging.basicConfig(level=logging.INFO, format='%(asctime)s - %(name)s - %(levelname)s - %(message)s', filename='dev.log')

info('Loading dependencies...')
from telegram import Update, ReplyKeyboardMarkup, ReplyKeyboardRemove
from telegram.ext import Updater, Defaults, CallbackContext, CommandHandler, MessageHandler, ConversationHandler, Filters
# from models import User, Person
import schedule
import threading
import time
from datetime import date


class BirthdayBot:
    def __init__(self):
        info('Creating Bot instance...')
        self.bot = Updater('1566557347:AAEywMgyGlh-RizkmQMr0xIRAvuq4sGgBuA')
        
        self.dispatcher = self.bot.dispatcher

        self.dispatcher.add_handler(CommandHandler('start', self.Start))
        self.dispatcher.add_handler(MessageHandler(Filters.all, self.Parrot))
    
    def Parrot(self, update:Update, context:CallbackContext):
        info(f'Got message from user { update.message.from_user.first_name } { update.message.from_user.last_name } ({ update.message.from_user.id }) - "{ update.message.text }"')
        update.message.reply_text(
            update.message.text
        )
    
    def Start(self, update:Update, context:CallbackContext):
        info(f'Got start message from { update.message.from_user.first_name } { update.message.from_user.last_name } ({ update.message.from_user.id })')
        update.message.reply_text(
            '*Привет я Пётр!*\n\n'
            '`Я могу напомнить тебе о важных днях!`',
            parse_mode='MarkdownV2',
        )


    def run(self):
        info('Starting bot...')
        self.bot.start_polling()
        self.bot.idle()


if __name__ == "__main__":
    bot = BirthdayBot()
    bot.run()
